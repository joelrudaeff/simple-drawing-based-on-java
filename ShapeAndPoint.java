import javafx.scene.shape.Shape;

public class ShapeAndPoint {
    private Shape shape;
    private Point point;

    public ShapeAndPoint(Shape shape, Point point) {
        this.shape = shape;
        this.point = point;
    }

    public Shape getShape() {
        return shape;
    }

    public Point getPoint() {
        return point;
    }
 }