import java.util.ArrayList;
import java.util.NoSuchElementException;
import javafx.event.ActionEvent;
import javafx.scene.shape.*;
import javafx.scene.paint.Color;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

public class DrawController {
	private ArrayList<ShapeAndPoint> shapesArray = new ArrayList<ShapeAndPoint>();
	private ShapesEnum currentShape = ShapesEnum.LINE;
	private Color currentColor = Color.BLACK;
	// True - filling color in the shape itself
	private boolean fillingMode = true;
	
    @FXML
    private Button clearButton;

    @FXML
    private Button colorButton;

    @FXML
    private Button fillShapeButton;

    @FXML
    private Pane pane;

    @FXML
    private Button shapeButton;

    @FXML
    private Button undoButton;
    
    private void createNewShape(Point startPoint) {
    	Shape tempShape;
    	
    	switch (currentShape) {
	    	case CIRCLE:
	    		tempShape = new Circle(startPoint.getX(), startPoint.getY(), 0);
	    		break;
	    	case RECTANGLE:
	    		tempShape = new Rectangle(startPoint.getX(), startPoint.getY(), 0, 0);
	    		break;
	    	// default is a Line
			default:
				tempShape = new Line(startPoint.getX(), startPoint.getY(), startPoint.getX(), startPoint.getY());
				break;
    	}
    
    	Utils.colorShape(tempShape, currentColor, fillingMode);
    	shapesArray.add(new ShapeAndPoint(tempShape,
    			new Point(startPoint.getX(), startPoint.getY())));
        pane.getChildren().add(tempShape);
    }
   
    @FXML
    void paneMousePressed(MouseEvent event) {
        createNewShape(new Point(event.getX(), event.getY()));
    }
    
    @FXML
    void paneMouseDragged(MouseEvent event) {
    	ShapeAndPoint lastSAndP = shapesArray.get(shapesArray.size() - 1);
		ShapeUtils.modifyLastShape(lastSAndP.getShape(), lastSAndP.getPoint(),
				new Point(event.getX(), event.getY()), currentColor, fillingMode);
    }
  
    @FXML
    void clearClicked(ActionEvent event) {
    	shapesArray.clear();
    	pane.getChildren().clear();
    }

    @FXML
    void colorClicked(ActionEvent event) {
    	try {
    		currentColor = Utils.changeColorBasedOnUser(Utils.getUserInputByDialog("Select A Color", 
            		"Please write your desired color, for example (one color at a time): black",
            		""));    		
    	}
    	catch (NoSuchElementException e1) {
    		// skip - user pressed escape without choosing
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}
    }

    @FXML
    void fillShapeClicked(ActionEvent event) {
    	Button fillButton = (Button)event.getSource();
    	
    	if (fillingMode) {
    		fillButton.setText("Not Filling color");
    		fillingMode = false;
    	}
    	else {
    		fillButton.setText("Filling color");
    		fillingMode = true;
    	}
    }

    @FXML
    void shapeClicked(ActionEvent event) {
    	try {
        	currentShape = Utils.changeShapeBasedOnUser(Utils.getUserInputByDialog("Select A Shape",
        			"Please write your desired shape from the list below",
        			"line, circle, rectangle"));   		
    	}
    	catch (NoSuchElementException e1) {
    		// skip - user pressed escape without choosing
    	}
    	catch (Exception e) {
    		e.printStackTrace();
    	}

    }

    @FXML
    void undoClicked(ActionEvent event) {
    	try {
    		if (shapesArray.size() > 0) {    
    			ShapeAndPoint lastSAndP = shapesArray.get(shapesArray.size() - 1); 
    			Shape lastShape = lastSAndP.getShape(); 
    			shapesArray.remove(shapesArray.size() - 1);
    			pane.getChildren().remove(lastShape);
    		}
    	}
    	catch (Exception  e) {
    		e.printStackTrace();
    	}
    }
}
