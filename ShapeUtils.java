import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class ShapeUtils {
	public static void modifyCircle(Circle lastShapeCircle, double startX, double startY, double endX, double endY) {
		// Calculate new center point and radius
        double centerX = (startX + endX) / 2;
        double centerY = (startY + endY) / 2;
        double radius = Math.sqrt(Math.pow(endX - startX, 2) + Math.pow(endY - startY, 2)) / 2;
        
        // On creation of a circle we give as input the center x,y and the radius
        lastShapeCircle.setCenterX(centerX);
        lastShapeCircle.setCenterY(centerY);
        lastShapeCircle.setRadius(radius);
	    }
	    
	public static void modifyRectangle(Rectangle lastShapeRectangle,double startX, double startY, double endX, double endY) {
        // Calculate width and height
        double width = Math.abs(endX - startX);
        double height = Math.abs(endY - startY);

        // Update dimensions
        lastShapeRectangle.setWidth(width);
        lastShapeRectangle.setHeight(height);

        // Update position based on the direction of dragging
        if (endX < startX) {
        	lastShapeRectangle.setX(endX);
        }
        if (endY < startY) {
        	lastShapeRectangle.setY(endY);
        }
    }
	    
    public static void modifyLine(Line lastShapeLine, double startX, double startY, double endX, double endY) {
    	lastShapeLine.setEndX(endX);
    	lastShapeLine.setEndY(endY);
    }
    
    public static void modifyLastShape(Shape lastShape, Point startPoint, Point endPoint, Color currentColor, boolean fillingMode) {	
    	Utils.colorShape(lastShape, currentColor, fillingMode);
    
    	if (lastShape instanceof Circle) {
    		ShapeUtils.modifyCircle((Circle)lastShape, startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
    	}
    	
    	else if (lastShape instanceof Rectangle) {
    		ShapeUtils.modifyRectangle((Rectangle)lastShape, startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
    	}
    	
    	// Line
    	else{
    		ShapeUtils.modifyLine(((Line)lastShape), startPoint.getX(), startPoint.getY(), endPoint.getX(), endPoint.getY());
    	}
    }
}