import javafx.scene.control.TextInputDialog;
import javafx.scene.paint.Color;
import javafx.scene.shape.Shape;

public class Utils {
    public static void colorShape(Shape tempShape, Color currentColor, boolean fillingMode) {
    	/*
    	 * Applies coloring according to the user prefences (with fill color or not)
    	 */
    	tempShape.setStroke(currentColor);
    	if (fillingMode) {
    		tempShape.setFill(currentColor);    		
    	}
    	else {
    		tempShape.setFill(Color.TRANSPARENT);
    	}
    }
    
    public static Color changeColorBasedOnUser(String userAnswer) {
    	// Tries to match to an existing color inside "Color"
    	// If an error rises - default is black (without showing an error alert to the user)
    	try {
    		return Color.valueOf(userAnswer.trim().toUpperCase());    		
    	}
    	catch (Exception e){
    		return Color.BLACK;    		
    	}
    	
    }
    
    public static ShapesEnum changeShapeBasedOnUser(String userAnswer) {
    	for (ShapesEnum ishape : ShapesEnum.values()) {
    		if (ishape.name().toLowerCase().equals(userAnswer.trim().toLowerCase())) {
    			return ShapesEnum.valueOf(ishape.name());
    		}
    	}
    	
    	// Default is a Line
    	return ShapesEnum.LINE;
    }
    
    public static String getUserInputByDialog(String title, String headerText, String contentText) {
        TextInputDialog dialog = new TextInputDialog();
        dialog.setTitle(title);
        dialog.setHeaderText(headerText);
		dialog.setContentText(contentText);    	
		return dialog.showAndWait().get();
    }
}
